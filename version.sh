#!/bin/bash

CHART_FILE="deeeplom-app/Chart.yaml"

# Извлечь строку, содержащую appVersion
APP_VERSION_LINE=$(grep 'appVersion:' "$CHART_FILE") && APP_VERSION=$(echo "$APP_VERSION_LINE" | awk '{print $2}') && APP_VERSION=$(echo "$APP_VERSION" | sed 's/"//g')

echo "appVersion: $APP_VERSION"